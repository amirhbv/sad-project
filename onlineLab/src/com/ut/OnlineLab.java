package com.ut;

import com.ut.exception.NotFoundAppropriateLab;
import com.ut.exception.NotFoundUserInsurance;
import com.ut.insurance.Insurance;
import com.ut.lab.Lab;
import com.ut.request.TestRequest;
import com.ut.test.Test;
import com.ut.test.TestType;
import com.ut.time.SamplingTime;
import com.ut.user.Patient;

import java.util.ArrayList;

public class OnlineLab {
    private static OnlineLab instance;
    ArrayList<Lab> labs;
    ArrayList<Patient> patients;
    ArrayList<TestType> testTypes;
    ArrayList<TestRequest> testRequests;
    ArrayList<Insurance> insurances;

    public static OnlineLab getInstance() {
        if (instance == null) {
            instance = new OnlineLab();
        }
        return instance;
    }

    public OnlineLab() {
        labs = new ArrayList<>();
        patients = new ArrayList<>();
        testTypes = new ArrayList<>();
        testRequests = new ArrayList<>();
        insurances = new ArrayList<>();
        initializeLabs();
        initializePatients();
        initializeTestTypes();
        initializeInsurances();
        updateSamplingTimes();
    }

    public void addLab(String name, ArrayList<Test> labTests, String id, ArrayList<String> labInsurances) {
        Lab lab = new Lab(name, id);
        for (Test test : labTests) {
            lab.addTest(test);
        }
        for (String insuranceId : labInsurances) {
            lab.addInsurance(insuranceId);
        }
        labs.add(lab);
    }

    public void addTestType(String name) {
        TestType testType = new TestType(name);
        testTypes.add(testType);
    }

    public void addPatient(String name, String id) {
        Patient patient = new Patient(name, id);
        patient.setInsuranceId("2");
        patients.add(patient);
    }

    public void addInsurance(String name, String[] testTypes, String id, int discountPercent) {
        Insurance insurance = new Insurance(id, name, discountPercent);
        for (String testType : testTypes) {
            insurance.addSupportedTestType(testType);
        }
        insurances.add(insurance);
    }

    private void initializeLabs() {
        ArrayList<Test> labTests = new ArrayList<>();
        ArrayList<String> labInsurances = new ArrayList<>();
        labTests.add(new Test("test1", 10000));
        labInsurances.add("1");
        addLab("lab1", labTests, "1", labInsurances);
        labTests.add(new Test("test2", 20000));
        labInsurances.add("2");
        addLab("lab2", labTests, "2", labInsurances);
        labTests.add(new Test("test3", 30000));
        labInsurances.add("3");
        addLab("lab3", labTests, "3", labInsurances);
    }

    private void initializePatients() {
        addPatient("patient1", "1");
    }

    private void initializeInsurances() {
        String[] testTypes = { "test1" };
        addInsurance("Insurance1", testTypes, "1", 10);
        testTypes = new String[] { "test1", "test2" };
        addInsurance("Insurance2", testTypes, "2", 20);
        testTypes = new String[] { "test1", "test2", "test3" };
        addInsurance("Insurance3", testTypes, "3", 30);
    }

    private void initializeTestTypes() {
        addTestType("test1");
        addTestType("test2");
        addTestType("test3");
    }

    private void updateSamplingTimes() {
        // ? Periodic task (weekly)
        for (Lab lab : labs) {
            lab.updateSamplingTimes();
        }
    }

    public String createRequest() {
        String requestId = "1";
        TestRequest testRequest = new TestRequest(requestId);
        testRequest.setUserId("1");
        testRequests.add(testRequest);
        return requestId;
    }

    public ArrayList<TestType> getTestTypes() {
        return testTypes;
    }

    public ArrayList<Lab> findAppropriateLabs(ArrayList<TestType> chosenTestTypes) throws NotFoundAppropriateLab {
        ArrayList<Lab> appropriateLabs = new ArrayList<>();
        for (Lab lab : labs) {
            if (lab.doesSupportTests(chosenTestTypes)) {
                appropriateLabs.add(lab);
            }
        }
        if(appropriateLabs.size() == 0){
            throw new NotFoundAppropriateLab();
        }
        return appropriateLabs;
    }

    public void chooseLab(Lab lab, String requestId, ArrayList<TestType> selectedTestTypes) {
        TestRequest testRequest = findTestRequest(requestId);
        if (testRequest != null) {
            testRequest.setLabId(lab.getId());
            testRequest.setTests(lab.getEquivalentTests(selectedTestTypes));
        }
    }

    public ArrayList<Insurance> findAppropriateInsurances(String requestId) {
        ArrayList<Insurance> labInsurances = new ArrayList<>();
        TestRequest testRequest = findTestRequest(requestId);
        if (testRequest != null) {
            Lab lab = findLab(testRequest.getLabId());
            ArrayList<String> labInsurancesId = lab.getInsurances();
            for (String insuranceId : labInsurancesId) {
                Insurance insurance = findInsurance(insuranceId);
                labInsurances.add(insurance);
            }
        }
        return labInsurances;

    }

    private Lab findLab(String id) {
        for (Lab lab : labs) {
            if (lab.getId().equals(id)) {
                return lab;
            }
        }
        return null;
    }

    private TestRequest findTestRequest(String id) {
        for (TestRequest testRequest : testRequests) {
            if (testRequest.getId().equals(id)) {
                return testRequest;
            }
        }
        return null;
    }

    public Insurance findInsurance(String id) {
        for (Insurance insurance : insurances) {
            if (insurance.getId().equals(id)) {
                return insurance;
            }
        }
        return null;
    }

    public void chooseInsurance(Insurance insurance, String requestId) throws NotFoundUserInsurance {
        TestRequest testRequest = findTestRequest(requestId);
        if (patients.get(0).doesHaveInsurance(insurance.getId())) {
            if (testRequest != null) {
                testRequest.setInsuranceId(insurance.getId());
            }
        }else{
            throw new NotFoundUserInsurance();
        }
    }

    public int getRequestPrice(String requestId) {
        TestRequest testRequest = findTestRequest(requestId);
        if (testRequest != null) {
            return testRequest.calculateTotalPrice();
        }
        return -1;
    }

    public ArrayList<SamplingTime> getFreeSamplingTimes(String requestId) {
        TestRequest testRequest = findTestRequest(requestId);
        Lab lab = findLab(testRequest.getLabId());
        return lab.getFreeSamplingTimes();
    }

    public void chooseSamplingTime(String requestId, SamplingTime samplingTime) {
        if (samplingTime.isUsed()) {
            // TODO: throw?
            System.out.println("Used SamplingTime");
        } else {
            TestRequest testRequest = findTestRequest(requestId);
            Lab lab = findLab(testRequest.getLabId());
            lab.setSamplingTime(testRequest, samplingTime);
        }
    }

    public void pay(String requestId) {
        TestRequest testRequest = findTestRequest(requestId);
        if (testRequest != null) {
            testRequest.setStatus("Submit a request");
        }
    }
}
