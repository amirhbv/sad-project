package com.ut.time;

import java.sql.Time;
import java.time.DayOfWeek;
import java.time.Duration;

public class ContractTime {
    DayOfWeek dayOfWeek;
    Time start;
    Duration duration;

    public ContractTime(DayOfWeek dayOfWeek, Time start, Duration duration) {
        this.dayOfWeek = dayOfWeek;
        this.start = start;
        this.duration = duration;
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public Time getStart() {
        return start;
    }

    public Duration getDuration() {
        return duration;
    }

    Boolean hasOverlap(ContractTime other) {
        return other.dayOfWeek == this.dayOfWeek && Duration.between(this.start.toInstant(), other.start.toInstant())
                .toMinutes() < this.duration.toMinutes();
    }
}
