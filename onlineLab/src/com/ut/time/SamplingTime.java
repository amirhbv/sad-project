package com.ut.time;

import java.util.Date;

public class SamplingTime {
    private Date start;
    private long durationMinute;
    private boolean isUsed;

    public SamplingTime(Date start, long durationMinute) {
        this.durationMinute = durationMinute;
        this.start = start;
        this.isUsed = false;
    }

    public boolean isUsed() {
        return isUsed;
    }

    public void setIsUsed() {
        this.isUsed = true;
    }

    public Date getStart() {
        return start;
    }

    public long getDurationMinute() {
        return durationMinute;
    }
}
