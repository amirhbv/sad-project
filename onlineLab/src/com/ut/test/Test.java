package com.ut.test;

public class Test extends TestType {
    private int price;

    public Test(String name, int price) {
        super(name);
        this.price = price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }
}
