package com.ut;

import com.ut.exception.NotFoundAppropriateLab;
import com.ut.exception.NotFoundUserInsurance;
import com.ut.insurance.Insurance;
import com.ut.lab.Lab;
import com.ut.test.TestType;
import com.ut.time.SamplingTime;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * App
 */
public class App {
    public static void main(final String[] args) {
        OnlineLab onlineLab = OnlineLab.getInstance();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Welcome to our Online Laboratory!");
            System.out.println("0 - Create new test request");
            System.out.println("1 - Exit");
            System.out.println("Enter number of what you want :}");
            String answer = scanner.nextLine();
            String requestId = "";
            if (Integer.valueOf(answer) == 0) {
                requestId = onlineLab.createRequest();
            } else {
                break;
            }

            System.out.println("Here are all the test types we support :");
            ArrayList<TestType> supportingTestTypes = onlineLab.getTestTypes();
            for (int i = 0; i < supportingTestTypes.size(); i++) {
                System.out.println(String.valueOf(i) + " - " + supportingTestTypes.get(i).getName());
            }
            System.out.println("Please enter the number of tests you selected [with space as delimiter]");
            answer = scanner.nextLine();
            String[] testNumbers = answer.split(" ");
            ArrayList<TestType> chosenTestTypes = new ArrayList<>();
            for (String s : testNumbers) {
                chosenTestTypes.add(supportingTestTypes.get(Integer.valueOf(s)));
            }
            ArrayList<Lab> appropriateLabs = null;
            try {
                appropriateLabs = onlineLab.findAppropriateLabs(chosenTestTypes);
                System.out.println("Here are all the labs that support your tests :");
                for (int i = 0; i < appropriateLabs.size(); i++) {
                    System.out.println(String.valueOf(i) + " - " + appropriateLabs.get(i).getName());
                }
                System.out.println("Please enter the laboratory number you selected");
                answer = scanner.nextLine();
                onlineLab.chooseLab(appropriateLabs.get(Integer.valueOf(answer)), requestId, chosenTestTypes);
                ArrayList<Insurance> appropriateInsurances = onlineLab.findAppropriateInsurances(requestId);

                System.out.println("Here are all the insurances that work with the lab you have chosen :");
                for (int i = 0; i < appropriateInsurances.size(); i++) {
                    System.out.println(String.valueOf(i) + " - " + appropriateInsurances.get(i).getName());
                }
                System.out.println("Please enter the insurance number you selected");
                answer = scanner.nextLine();
                try {
                    onlineLab.chooseInsurance(appropriateInsurances.get(Integer.valueOf(answer)), requestId);
                } catch (NotFoundUserInsurance notFoundUserInsurance) {
                    System.out.println("Your insurance is not valid.");
                }
                int totalPrice = onlineLab.getRequestPrice(requestId);

                System.out.println("Total price = " + totalPrice);
                ArrayList<SamplingTime> freeSamplingTimes = onlineLab.getFreeSamplingTimes(requestId);
                System.out.println("Choose the time you want to sample from the following times :");
                for (int i = 0; i < freeSamplingTimes.size(); i++) {
                    System.out.println(String.valueOf(i) + " - " + freeSamplingTimes.get(i).getStart().toString());
                    System.out.println("    Duration : " + freeSamplingTimes.get(i).getDurationMinute());
                }
                answer = scanner.nextLine();
                onlineLab.chooseSamplingTime(requestId, freeSamplingTimes.get(Integer.valueOf(answer)));
                System.out.println("Please pay the amount of " + totalPrice);
                onlineLab.pay(requestId);

                System.out.println("Your request has been submitted.");
            } catch (NotFoundAppropriateLab notFoundAppropriateLab) {
                System.out.println("There is'nt any lab can support all these tests.");
            }

            System.out.println("0 - Continue");
            System.out.println("1 - Exit");
            answer = scanner.nextLine();
            if (Integer.valueOf(answer) == 1) {
                break;
            }
        }
        scanner.close();
    }
}
