package com.ut.insurance;

import com.ut.test.TestType;

import java.util.ArrayList;

public class Insurance {
    private String id;
    private String name;
    private ArrayList<TestType> supportedTestTypes;
    private int discountPercent;

    public Insurance(String id, String name, int discountPercent) {
        this.supportedTestTypes = new ArrayList<>();
        this.id = id;
        this.name = name;
        this.discountPercent = discountPercent;
    }

    public void addSupportedTestType(String name) {
        TestType testType = new TestType(name);
        supportedTestTypes.add(testType);
    }

    public String getId() {
        return id;
    }

    public ArrayList<TestType> getSupportedTestTypes() {
        return supportedTestTypes;
    }

    public String getName() {
        return name;
    }

    public boolean doesSupport(String testName) {
        for (TestType testType : supportedTestTypes) {
            if (testType.getName().equals(testName)) {
                return true;
            }
        }
        return false;
    }

    public int getDiscount(int price) {
        return price * discountPercent / 100;
    }
}
