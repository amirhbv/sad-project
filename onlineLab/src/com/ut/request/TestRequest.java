package com.ut.request;

import com.ut.OnlineLab;
import com.ut.insurance.Insurance;
import com.ut.test.Test;
import com.ut.time.SamplingTime;

import java.util.ArrayList;

public class TestRequest {
    private String status;
    private String id;
    private int totalPrice;
    private String labId;
    private ArrayList<Test> tests;
    private String insuranceId;
    private String userId;
    private SamplingTime samplingTime;

    public TestRequest(String id) {
        this.status = "In Progress";
        this.totalPrice = 0;
        this.tests = new ArrayList<>();
        this.id = id;
        this.insuranceId = "";
    }

    public void setSamplingTime(SamplingTime samplingTime) {
        this.samplingTime = samplingTime;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTests(ArrayList<Test> tests) {
        this.tests = tests;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public String getLabId() {
        return labId;
    }

    public void setInsuranceId(String insuranceId) {
        this.insuranceId = insuranceId;
    }

    public int calculateTotalPrice() {
        this.totalPrice = 0;
        for (Test test : tests) {
            totalPrice += test.getPrice();
            if (!insuranceId.equals("")) {
                Insurance insurance = OnlineLab.getInstance().findInsurance(insuranceId);
                if (insurance.doesSupport(test.getName())) {
                    totalPrice -= insurance.getDiscount(test.getPrice());
                }
            }
        }
        return totalPrice;
    }

}
