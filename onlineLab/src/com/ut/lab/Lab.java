package com.ut.lab;

import com.ut.request.TestRequest;
import com.ut.test.Test;
import com.ut.test.TestType;
import com.ut.time.ContractTime;
import com.ut.time.SamplingTime;
import com.ut.user.Sampler;

import java.time.DayOfWeek;
import java.time.Duration;
import java.util.ArrayList;

public class Lab {
    private String name;
    private ArrayList<Test> tests;
    private String id;
    private ArrayList<String> insurances;
    private ArrayList<Sampler> samplers;

    public Lab(String name, String id) {
        this.tests = new ArrayList<>();
        this.insurances = new ArrayList<>();
        this.samplers = new ArrayList<>();
        this.name = name;
        this.id = id;
        initialSamplers();
    }

    public void initialSamplers() {
        ArrayList<ContractTime> contractTimes1 = new ArrayList<>();
        contractTimes1.add(new ContractTime(DayOfWeek.SATURDAY, new java.sql.Time(8, 0, 0), Duration.ofMinutes(120)));
        contractTimes1.add(new ContractTime(DayOfWeek.SUNDAY, new java.sql.Time(8, 0, 0), Duration.ofMinutes(120)));
        contractTimes1.add(new ContractTime(DayOfWeek.MONDAY, new java.sql.Time(8, 0, 0), Duration.ofMinutes(120)));
        contractTimes1.add(new ContractTime(DayOfWeek.TUESDAY, new java.sql.Time(8, 0, 0), Duration.ofMinutes(120)));
        addSampler("sampler1", "1", contractTimes1);

        ArrayList<ContractTime> contractTimes2 = new ArrayList<>();
        contractTimes2.add(new ContractTime(DayOfWeek.WEDNESDAY, new java.sql.Time(8, 0, 0), Duration.ofMinutes(60)));
        contractTimes2.add(new ContractTime(DayOfWeek.WEDNESDAY, new java.sql.Time(9, 0, 0), Duration.ofMinutes(60)));
        contractTimes2.add(new ContractTime(DayOfWeek.WEDNESDAY, new java.sql.Time(10, 0, 0), Duration.ofMinutes(60)));
        contractTimes2.add(new ContractTime(DayOfWeek.WEDNESDAY, new java.sql.Time(11, 0, 0), Duration.ofMinutes(60)));
        addSampler("sampler2", "2", contractTimes2);
    }

    public void addSampler(String name, String id, ArrayList<ContractTime> contractTimes) {
        samplers.add(new Sampler(name, id, this, contractTimes));
    }

    public ArrayList<SamplingTime> getAllSamplingTimes() {
        ArrayList<SamplingTime> samplingTimes = new ArrayList<>();
        for (Sampler sampler : samplers) {
            samplingTimes.addAll(sampler.getSamplingTimes());
        }
        return samplingTimes;
    }

    public ArrayList<SamplingTime> getFreeSamplingTimes() {
        ArrayList<SamplingTime> freeTimes = new ArrayList<>();
        for (SamplingTime time : getAllSamplingTimes()) {
            if (!time.isUsed()) {
                freeTimes.add(time);
            }
        }
        return freeTimes;
    }

    public String getName() {
        return name;
    }

    public void addTest(Test test) {
        tests.add(test);
    }

    public void addInsurance(String id) {
        insurances.add(id);
    }

    private boolean doesSupportTest(TestType testType) {
        for (Test test : tests) {
            if (test.getName().equals(testType.getName())) {
                return true;
            }
        }
        return false;
    }

    public boolean doesSupportTests(ArrayList<TestType> testTypes) {
        for (TestType testType : testTypes) {
            if (!doesSupportTest(testType)) {
                return false;
            }
        }
        return true;
    }

    public String getId() {
        return id;
    }

    public ArrayList<Test> getEquivalentTests(ArrayList<TestType> testTypes) {
        ArrayList<Test> equivalentTests = new ArrayList<>();
        for (TestType testType : testTypes) {
            for (Test test : tests) {
                if (test.getName().equals(testType.getName())) {
                    equivalentTests.add(test);
                }
            }
        }
        return equivalentTests;
    }

    public ArrayList<String> getInsurances() {
        return insurances;
    }

    public void setSamplingTime(TestRequest request, SamplingTime samplingTime) {
        request.setSamplingTime(samplingTime);
        samplingTime.setIsUsed();
    }

    public void updateSamplingTimes() {
        for (Sampler sampler : samplers) {
            sampler.updateSamplingTimes();
        }
    }
}
