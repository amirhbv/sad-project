package com.ut.user;

/**
 * Patient
 */
public class Patient extends User {
    private String insuranceId = "";

    public Patient(String fullName, String id) {
        super(fullName, id);
    }

    public void setInsuranceId(String insuranceId) {
        this.insuranceId = insuranceId;
    }

    public boolean doesHaveInsurance(String insurance) {
        if (insuranceId.equals(insurance)) {
            return true;
        }
        return false;
    }
}
