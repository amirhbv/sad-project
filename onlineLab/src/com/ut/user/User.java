package com.ut.user;

public abstract class User {
    private String fullName;
    private String id;

    public User(String fullName, String id) {
        this.fullName = fullName;
        this.id = id;
    }

    public String getFullName() {
        return this.fullName;
    }

    public String getId() {
        return id;
    }
}
