package com.ut.user;

import com.ut.lab.Lab;

/**
 * Expert
 */
public class Expert extends User {
    private Lab lab;

    public Expert(String fullName, String id, Lab lab) {
        super(fullName, id);
        this.lab = lab;
    }

    public Lab getLab() {
        return lab;
    }
}
