package com.ut.user;

import java.sql.Time;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.ut.lab.Lab;
import com.ut.time.ContractTime;
import com.ut.time.SamplingTime;

/**
 * Sampler
 */
public class Sampler extends User {
    private Lab lab;
    private ArrayList<ContractTime> contractTimes;
    private ArrayList<SamplingTime> samplingTimes;

    public Sampler(String fullName, String id, Lab lab, ArrayList<ContractTime> contractTimes) {
        super(fullName, id);
        this.lab = lab;
        this.contractTimes = contractTimes;
        this.samplingTimes = new ArrayList<>();
    }

    public Lab getLab() {
        return lab;
    }

    public ArrayList<ContractTime> getContractTimes() {
        return contractTimes;
    }

    public ArrayList<SamplingTime> getSamplingTimes() {
        return samplingTimes;
    }

    public static final long MILLISECONDS_PER_WEEK = 7L * 24 * 60 * 60 * 1000;

    private static Date getDate(DayOfWeek dayOfWeek, Time time) {
        // Parse given date. Convert this to milliseconds since epoch. This will
        // result in a date during the first week of 1970.
        Date date = new SimpleDateFormat("E, H:mm")
                .parse(String.format("%s, %s", dayOfWeek.toString(), time.toString()), new ParsePosition(0));

        // Convert to millis and adjust for offset between today's Daylight
        // Saving Time (default for the sdf) and the 1970 date
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int todayDSTOffset = calendar.get(Calendar.DST_OFFSET);
        int epochDSTOffset = calendar.get(Calendar.DST_OFFSET);
        long parsedMillis = date.getTime() + (epochDSTOffset - todayDSTOffset);

        // Calculate how many weeks ago that was
        long millisInThePast = System.currentTimeMillis() - parsedMillis;
        long weeksInThePast = millisInThePast / MILLISECONDS_PER_WEEK;
        // Add that number of weeks plus 1
        return new Date(parsedMillis + (weeksInThePast + 1) * MILLISECONDS_PER_WEEK);
    }

    public void updateSamplingTimes() {
        for (ContractTime contractTime : contractTimes) {
            this.samplingTimes
                    .add(new SamplingTime(Sampler.getDate(contractTime.getDayOfWeek(), contractTime.getStart()),
                            contractTime.getDuration().toMinutes()));
        }
    }
}
